# RVGL Launcher

RVGL Launcher is a cross-platform installer, launcher and package manager for RVGL.
This is a planned successor to the linux-only [RVGL Installer](https://gitlab.com/re-volt/rvgl-installer)
script.

[![Install tab][1]][2]

[ [Game tab](https://re-volt.gitlab.io/rvgl-launcher/screens/launch.png) |
[Events tab](https://re-volt.gitlab.io/rvgl-launcher/screens/events.png) |
[Packs tab](https://re-volt.gitlab.io/rvgl-launcher/screens/packs.png) |
[Repositories tab](https://re-volt.gitlab.io/rvgl-launcher/screens/repos.png) |
[Local tab](https://re-volt.gitlab.io/rvgl-launcher/screens/local.png) |
[Console tab](https://re-volt.gitlab.io/rvgl-launcher/screens/console.png) ]

[1]: https://re-volt.gitlab.io/rvgl-launcher/screens/install.th.png
[2]: https://re-volt.gitlab.io/rvgl-launcher/screens/install.png

## Features

- Install and update game and content packs.
- View list of online events and join them directly.
- Enable or disable content packs on the fly.
- Add your own content repositories.

## Download

You can get prepackaged builds below:

- [RVGL Launcher for 32-bit Windows](https://rvgl.re-volt.io/downloads/rvgl_launcher_win32.zip)
- [RVGL Launcher for 64-bit Windows](https://rvgl.re-volt.io/downloads/rvgl_launcher_win64.zip)
- [RVGL Launcher for GNU/Linux](https://rvgl.re-volt.io/downloads/rvgl_launcher_linux.zip)

## Requirements

- Python 3.8 or above
- Python modules:
  - wxPython
  - requests
  - packaging
- 7-Zip

### Linux

Install required dependencies and run `./rvgl_launcher.py`.

### Windows

Run `rvgl_launcher.exe`.

