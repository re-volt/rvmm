import setuptools
import os

with open("README.md", "r", encoding="utf-8") as f:
    long_description = f.read()

content = {}
with open(os.path.join("rv_launcher", "version.py")) as f:
    exec(f.read(), content)

setuptools.setup(
    name="rvgl-launcher",
    version=content["__version__"],
    author="RV Team",
    author_email="contact@re-volt.io",
    description="Launcher and package manager for RVGL",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/re-volt/rvgl-launcher",
    packages=setuptools.find_packages(),
    py_modules=["rvgl_launcher"],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
        "Development Status :: 2 - Pre-Alpha"
    ],
    python_requires='>=3.8',
    install_requires=['wx', 'requests']
)
