# Installation

<!-- toc -->

[![Install tab](screens/install.th.png)](screens/install.png)

## Install Type

You can decide how you want to install the game, pick the install location or
even use an existing game install with the launcher. There are two available
install types:

- **Standard**: Select this for a clean, new install and let the launcher
manage your game data and packages. Game data is placed in the default location
in your user directory.

- **Custom**: Select this to pick your own install location. This can be either
an empty folder (for a new install) or your existing game folder. When pointing
to an existing install, your data and save files are automatically carried over
to the launcher.

## Game Presets

The launcher offers you a couple of presets, or `recipes`, during the
installation. Each preset is a different collection of packages.

- **Original**: Includes original assets and original soundtrack. Select this
if you want to have the stock game with no mods. You can always add more content
packs later.

- **Online**: Includes I/O content packs and a community made alternative
soundtrack. Select this if you want to play online and join sessions hosted
by the I/O community.

- **Basic**: Includes original assets and no soundtrack. Only select this if
you're low on bandwidth or you plan on adding your own music packs.

- **Custom**: Choose your own packs. This takes you to the Packs tab where you
can select the packs that make up your game installation.

The installation process can take some time, especially if you selected the
`Online` preset. So, go get some coffee!

> For a new install, be sure to enable at least the `game_files` and `rvgl_assets`
packs. Without these, the game might not function. For an existing install,
enable the `default` pack to use content present in the root game folder.

> Once the game is installed, you can easily create your own presets and switch
between them. See the [Packages page](./packs.md) for more information.

## Desktop Shortcut

Before installing, you have the option to create a Desktop Shortcut (on Windows)
or an App Menu Shortcut (on GNU/Linux).

## Updating Your Game

The Information dashboard shows you the number of packages that have updates
available. Press the `View` button to review packages and install updates.

You also have the option to update RVGL alone. To do this, use the `Update`
button next to the RVGL update notification.

## Repairing Your Game

In case something went wrong — say, the power went out during installation — 
and data was left partially installed or corrupted, you have a couple of
options under `File` menu.

- **Repair**: Cleans and reinstalls all packages. Cached download files are used
when available to save you bandwidth.

- **Clean**: Lets you delete unused package files and cached download files to
reclaim disk space. Use this if you suspect any of the downloaded packages are
corrupt and need to manually trigger a re-download (should almost never happen).

- **Uninstall**: Wipes out all game data. Have a fresh start. Cached download files
are preserved and will be used in case of a reinstall.

